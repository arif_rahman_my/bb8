# BB8

BB8 on Gazebo 9

## How to test

1. Clone the `kinetic` branch
2. `$ roslaunch bb_8_gazebo main.launch`
3. `$ gzclient --verbose`
4. `$ rosrun image_view image_view image:=/bb8/camera1/image_raw`
5. `$ roslaunch bb_8_teleop keyboard_teleop.launch`

